FROM openjdk:8-jdk
MAINTAINER "David Brama" <david.brama@methoda.com>

WORKDIR /opt

RUN apt-get install -y apt-transport-https
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - 
RUN apt-get install -y nodejs maven



RUN mkdir -p /usr/src/confapp
WORKDIR /usr/src/confapp
ADD atlassian-plugin-sdk_8.0.16_all.deb /usr/src/confapp/
RUN dpkg -i atlassian-plugin-sdk_8.0.16_all.deb 
RUN rm -f atlassian-plugin-sdk_8.0.16_all.deb
RUN atlas-create-confluence-plugin --non-interactive -a "tempaddon" -g "com.example.conf" -v "0.0.1-SNAPSHOT" -p "com.example.conf" -u "6.3.15"
WORKDIR /usr/src/confapp/tempaddon
RUN atlas-mvn package

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY settings.template.xml settings.template.xml 
COPY generate_settings.sh /usr/local/bin/generate_settings.sh
COPY validate-release-config.sh /usr/local/bin/validate-release-config.sh

RUN chmod +x /usr/local/bin/generate_settings.sh
RUN chmod +x /usr/local/bin/validate-release-config.sh
RUN npm config set cache /root/.npm --global

ENV PATH = /usr/src/app:$PATH


#CMD atlas-version && node -v
